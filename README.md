# WoloxTest
This project requires MySQL, I recommend uses docker:
```bash
docker run --rm -p 3306:3306 --name=mysql01 -e MYSQL_ROOT_HOST=% -e MYSQL_PASSWORD=toor -e MYSQL_ROOT_PASSWORD=toor -d mysql/mysql-server:8.0
```

Link to the API: http://localhost:8080/swagger-ui.html

- this project is 100% reactive using REACTOR
- This project is structured using DDD
- this project uses a Factory design pattern
- this project uses the dependency inversion pattern
- this project has a small example of uni-testing
- this project uses flyway for database migration
- this project runs an example pipeline for CI-CD. check Gitlab
