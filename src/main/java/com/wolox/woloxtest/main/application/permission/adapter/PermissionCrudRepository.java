package com.wolox.woloxtest.main.application.permission.adapter;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface PermissionCrudRepository extends ReactiveCrudRepository<AlbumPermissionDB, Integer> {

    public Flux<AlbumPermissionDB> findAll();

    public Mono<AlbumPermissionDB> save(AlbumPermissionDB albumPermissionDB);
}
