package com.wolox.woloxtest.main.application.permission.adapter;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class AlbumPermission {
    private Integer id;
    private Integer userId;
    private Integer albumId;
    private Boolean allowRead;
    private Boolean allowWrite;

}
