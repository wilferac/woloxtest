package com.wolox.woloxtest.main.application.album;

import com.wolox.woloxtest.main.application.album.adapter.AlbumAdapter;
import com.wolox.woloxtest.main.application.album.adapter.AlbumRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class AlbumFactory {

    @Bean
    public AlbumRepository getAlbumRepository(WebClient webClient) {
        return new AlbumAdapter(webClient);
    }

    @Bean
    public AlbumManager getAlbumManager(AlbumRepository albumRepository) {
        return new AlbumManager(albumRepository);
    }

}
