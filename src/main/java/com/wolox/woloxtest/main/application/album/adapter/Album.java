package com.wolox.woloxtest.main.application.album.adapter;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Album {
    private Long userId;
    private Long id;
    private String title;
    
}
