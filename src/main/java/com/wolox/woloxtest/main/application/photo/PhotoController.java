package com.wolox.woloxtest.main.application.photo;

import com.wolox.woloxtest.main.application.photo.adapter.Photo;
import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

@RestController
@AllArgsConstructor
public class PhotoController {
    private PhotoManager photoManager;

    @GetMapping("/photos")
    @Operation(summary = "Get all the photos")
    public Flux<Photo> getAllPhotos() {
        return photoManager.getAllPhotos();
    }

    @GetMapping("/photos/user/{userId}")
    @Operation(summary = "Get all the albums")
    public Flux<Photo> getPhotosByUser(@PathVariable Long userId) {
        return photoManager.getPhotosByUser(userId);
    }

}
