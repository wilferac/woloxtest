package com.wolox.woloxtest.main.application.permission.adapter;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

@Data
@Getter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table("album_permission")
public class AlbumPermissionDB {

    @Id
    @Column("id")
    private Integer id;

    @Column("userId")
    private Integer userId;

    @Column("albumId")
    private Integer albumId;

    @Column("allowRead")
    private Boolean allowRead;

    @Column("allowWrite")
    private Boolean allowWrite;

}
