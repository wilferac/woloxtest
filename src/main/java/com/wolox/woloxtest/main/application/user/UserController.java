package com.wolox.woloxtest.main.application.user;

import com.wolox.woloxtest.main.application.user.adapter.User;
import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

@RestController
@AllArgsConstructor
public class UserController {
    private UserManager userManager;

    @GetMapping("/users")
    @Operation(summary = "Get all the users")
    public Flux<User> getAllUsers() {
        return userManager.getAllUsers();
    }

}
