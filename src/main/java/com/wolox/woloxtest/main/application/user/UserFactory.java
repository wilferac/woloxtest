package com.wolox.woloxtest.main.application.user;

import com.wolox.woloxtest.main.application.user.adapter.UserAdapter;
import com.wolox.woloxtest.main.application.user.adapter.UserRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class UserFactory {

    @Bean
    public UserRepository getUserRepository(WebClient webClient) {
        return new UserAdapter(webClient);
    }

    @Bean
    public UserManager getUserManager(UserRepository userRepository) {
        return new UserManager(userRepository);
    }

}
