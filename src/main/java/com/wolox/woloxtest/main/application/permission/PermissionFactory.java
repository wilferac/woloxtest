package com.wolox.woloxtest.main.application.permission;

import com.wolox.woloxtest.main.application.permission.adapter.PermissionAdapter;
import com.wolox.woloxtest.main.application.permission.adapter.PermissionCrudRepository;
import com.wolox.woloxtest.main.application.permission.adapter.PermissionRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PermissionFactory {

    @Bean
    public PermissionRepository getPermissionRepository(PermissionCrudRepository permissionCrudRepository) {
        return new PermissionAdapter(permissionCrudRepository);
    }

    @Bean
    public PermissionManager getPermissionManager(PermissionRepository permissionRepository) {
        return new PermissionManager(permissionRepository);
    }

}
