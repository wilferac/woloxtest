package com.wolox.woloxtest.main.application.photo.adapter;

import lombok.AllArgsConstructor;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

@AllArgsConstructor
public class PhotoAdapter implements PhotoRepository {
    private WebClient webClient;

    @Override
    public Flux<Photo> getAllPhotos() {
        return webClient.get()
                .uri("https://jsonplaceholder.typicode.com/photos")
                .retrieve()
                .bodyToFlux(Photo.class);
    }

}
