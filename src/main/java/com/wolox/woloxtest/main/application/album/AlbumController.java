package com.wolox.woloxtest.main.application.album;

import com.wolox.woloxtest.main.application.album.adapter.Album;
import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

@RestController
@AllArgsConstructor
public class AlbumController {
    private AlbumManager albumManager;

    @GetMapping("/albums")
    @Operation(summary = "Get all the albums")
    public Flux<Album> getAllAlbums() {
        return albumManager.getAllAlbums();
    }

    @GetMapping("/albums/user/{userId}")
    @Operation(summary = "Get all the albums")
    public Flux<Album> getAllAlbums(@PathVariable Long userId) {
        return albumManager.getAlbumsByUser(userId);
    }

}
