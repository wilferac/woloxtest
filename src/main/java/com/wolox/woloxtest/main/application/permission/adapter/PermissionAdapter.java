package com.wolox.woloxtest.main.application.permission.adapter;

import lombok.AllArgsConstructor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@AllArgsConstructor
public class PermissionAdapter implements PermissionRepository {
    private PermissionCrudRepository permissionRepository;

    @Override
    public Flux<AlbumPermission> getAllPermissions() {
        return permissionRepository.findAll().map(this::mapAlbumPermission);
    }

    @Override
    public Mono<AlbumPermission> savePermission(AlbumPermission albumPermission) {
        return permissionRepository.save(mapAlbumPermissionDB(albumPermission).build()).map(this::mapAlbumPermission);
    }

    @Override
    public Mono<AlbumPermission> editPermission(AlbumPermission albumPermission) {
        return permissionRepository.save(mapAlbumPermissionDB(albumPermission).id(albumPermission.getId()).build()).map(this::mapAlbumPermission);
    }

    private AlbumPermission mapAlbumPermission(AlbumPermissionDB albumPermissionDB) {
        return AlbumPermission.builder()
                .id(albumPermissionDB.getId())
                .userId(albumPermissionDB.getUserId())
                .albumId(albumPermissionDB.getAlbumId())
                .allowRead(albumPermissionDB.getAllowRead())
                .allowWrite(albumPermissionDB.getAllowWrite())
                .build();
    }

    private AlbumPermissionDB.AlbumPermissionDBBuilder mapAlbumPermissionDB(AlbumPermission albumPermission) {
        return AlbumPermissionDB.builder()
                .userId(albumPermission.getUserId())
                .albumId(albumPermission.getAlbumId())
                .allowRead(albumPermission.getAllowRead())
                .allowWrite(albumPermission.getAllowWrite());
    }

}
