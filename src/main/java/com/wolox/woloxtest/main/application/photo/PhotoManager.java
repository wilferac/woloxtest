package com.wolox.woloxtest.main.application.photo;

import com.wolox.woloxtest.main.application.album.AlbumManager;
import com.wolox.woloxtest.main.application.album.adapter.Album;
import com.wolox.woloxtest.main.application.photo.adapter.Photo;
import com.wolox.woloxtest.main.application.photo.adapter.PhotoRepository;
import lombok.AllArgsConstructor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@AllArgsConstructor
public class PhotoManager {
    private PhotoRepository photoRepository;
    private AlbumManager albumManager;

    public Flux<Photo> getAllPhotos() {
        return photoRepository.getAllPhotos();
    }

    public Flux<Photo> getPhotosByUser(Long userId) {
        Mono<Map<Long, Album>> userAlbums = albumManager.getAlbumsByUser(userId).collectMap(album -> album.getId());
        Mono<List<Photo>> allPhotos = photoRepository.getAllPhotos().collectList();

        return Mono.zip(userAlbums, allPhotos).map(t -> {
                return t.getT2().stream()
                        .filter(photo -> t.getT1().containsKey(photo.getAlbumId()))
                        .collect(Collectors.toList());
        }).flatMapIterable(e -> e);
    }
}
