package com.wolox.woloxtest.main.application.permission;

import com.wolox.woloxtest.main.application.permission.adapter.AlbumPermission;
import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@AllArgsConstructor
public class PermissionController {
    private PermissionManager permissionManager;

    @GetMapping("/getPermissions")
    @Operation(summary = "Get all the permissions")
    public Flux<AlbumPermission> getAllPhotos() {
        return permissionManager.getAllPermissions();
    }

    @PostMapping("/permission")
    @Operation(summary = "Save the new permission")
    public Mono<AlbumPermission> savePermission(@RequestBody AlbumPermission albumPermission) {
        return permissionManager.savePermission(albumPermission);
    }

    @PutMapping("/permission")
    @Operation(summary = "Edit the permission")
    public Mono<AlbumPermission> editPermission(@RequestBody AlbumPermission albumPermission) {
        return permissionManager.editPermission(albumPermission);
    }

}
