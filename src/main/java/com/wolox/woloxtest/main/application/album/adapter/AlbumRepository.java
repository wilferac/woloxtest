package com.wolox.woloxtest.main.application.album.adapter;

import reactor.core.publisher.Flux;

public interface AlbumRepository {

    public Flux<Album> getAllAlbums();
}
