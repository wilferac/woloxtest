package com.wolox.woloxtest.main.application.user.adapter;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class User {
    private Integer id;
    private String name;
}
