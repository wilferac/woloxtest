package com.wolox.woloxtest.main.application.permission;

import com.wolox.woloxtest.main.application.permission.adapter.AlbumPermission;
import com.wolox.woloxtest.main.application.permission.adapter.PermissionRepository;
import lombok.AllArgsConstructor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@AllArgsConstructor
public class PermissionManager {
    private PermissionRepository permissionRepository;

    public Flux<AlbumPermission> getAllPermissions() {
        return permissionRepository.getAllPermissions();
    }

    public Mono<AlbumPermission> savePermission(AlbumPermission albumPermission) {
        return permissionRepository.savePermission(albumPermission);
    }

    public Mono<AlbumPermission> editPermission(AlbumPermission albumPermission) {
        return permissionRepository.editPermission(albumPermission);
    }
}
