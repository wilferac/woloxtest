package com.wolox.woloxtest.main.application.permission.adapter;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface PermissionRepository {

    public Flux<AlbumPermission> getAllPermissions();

    Mono<AlbumPermission> savePermission(AlbumPermission albumPermission);

    Mono<AlbumPermission> editPermission(AlbumPermission albumPermission);
}
