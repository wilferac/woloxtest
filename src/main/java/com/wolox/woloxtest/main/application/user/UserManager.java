package com.wolox.woloxtest.main.application.user;

import com.wolox.woloxtest.main.application.user.adapter.User;
import com.wolox.woloxtest.main.application.user.adapter.UserRepository;
import lombok.AllArgsConstructor;
import reactor.core.publisher.Flux;

@AllArgsConstructor
public class UserManager {
    private UserRepository userRepository;

    public Flux<User> getAllUsers() {
        return userRepository.getAllUsers();
    }
}
