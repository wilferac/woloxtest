package com.wolox.woloxtest.main.application.user.adapter;

import reactor.core.publisher.Flux;

public interface UserRepository {

    public Flux<User> getAllUsers();
}
