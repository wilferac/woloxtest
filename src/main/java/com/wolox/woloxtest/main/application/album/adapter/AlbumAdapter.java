package com.wolox.woloxtest.main.application.album.adapter;

import lombok.AllArgsConstructor;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

@AllArgsConstructor
public class AlbumAdapter implements AlbumRepository {
    private WebClient webClient;

    @Override
    public Flux<Album> getAllAlbums() {
        return webClient.get()
                .uri("https://jsonplaceholder.typicode.com/albums")
                .retrieve()
                .bodyToFlux(Album.class);
    }

}
