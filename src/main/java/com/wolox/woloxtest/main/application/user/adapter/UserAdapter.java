package com.wolox.woloxtest.main.application.user.adapter;

import lombok.AllArgsConstructor;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

@AllArgsConstructor
public class UserAdapter implements UserRepository {
    private WebClient webClient;

    @Override
    public Flux<User> getAllUsers() {
        return webClient.get()
                .uri("https://jsonplaceholder.typicode.com/users")
                .retrieve()
                .bodyToFlux(User.class);
    }

}
