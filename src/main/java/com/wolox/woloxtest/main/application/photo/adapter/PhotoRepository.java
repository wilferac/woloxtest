package com.wolox.woloxtest.main.application.photo.adapter;

import reactor.core.publisher.Flux;

public interface PhotoRepository {

    public Flux<Photo> getAllPhotos();
}
