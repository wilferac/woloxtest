package com.wolox.woloxtest.main.application.photo;

import com.wolox.woloxtest.main.application.album.AlbumManager;
import com.wolox.woloxtest.main.application.photo.adapter.PhotoAdapter;
import com.wolox.woloxtest.main.application.photo.adapter.PhotoRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class PhotoFactory {

    @Bean
    public PhotoRepository getPhotoRepository(WebClient webClient) {
        return new PhotoAdapter(webClient);
    }

    @Bean
    public PhotoManager getPhotoManager(PhotoRepository photoRepository, AlbumManager albumManager) {
        return new PhotoManager(photoRepository, albumManager);
    }

}
