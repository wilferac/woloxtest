package com.wolox.woloxtest.main.application.photo.adapter;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Photo {
    private Long albumId;
    private Long id;
    private String title;
    private String url;
    private String thumbnailUrl;
}
