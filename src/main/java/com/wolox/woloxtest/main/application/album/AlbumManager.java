package com.wolox.woloxtest.main.application.album;

import com.wolox.woloxtest.main.application.album.adapter.Album;
import com.wolox.woloxtest.main.application.album.adapter.AlbumRepository;
import lombok.AllArgsConstructor;
import reactor.core.publisher.Flux;

@AllArgsConstructor
public class AlbumManager {
    private AlbumRepository albumRepository;

    public Flux<Album> getAllAlbums() {
        return albumRepository.getAllAlbums();
    }

    public Flux<Album> getAlbumsByUser(Long userId) {
        return albumRepository.getAllAlbums()
                .filter(album -> album.getUserId().equals(userId));
    }
}
