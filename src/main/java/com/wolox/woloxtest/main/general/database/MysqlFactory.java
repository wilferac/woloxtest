package com.wolox.woloxtest.main.general.database;

import dev.miku.r2dbc.mysql.MySqlConnectionConfiguration;
import dev.miku.r2dbc.mysql.MySqlConnectionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.r2dbc.config.AbstractR2dbcConfiguration;
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories;

@Configuration
@EnableR2dbcRepositories
public class MysqlFactory extends AbstractR2dbcConfiguration {

    private String host = "localhost";

    private Integer port = 3306;

    private String username = "root";

    private String password = "toor";

    private String database = "testSchema";

    @Bean
    public MySqlConnectionFactory connectionFactory() {
        MySqlConnectionConfiguration configuration = MySqlConnectionConfiguration.builder()
                .host(host)
                .username(username)
                .port(port)
                .password(password)
                .database(database)
                .build();

        return MySqlConnectionFactory.from(configuration);
    }

}