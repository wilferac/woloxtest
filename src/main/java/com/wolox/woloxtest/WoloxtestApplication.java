package com.wolox.woloxtest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WoloxtestApplication {

    public static void main(String[] args) {
        SpringApplication.run(WoloxtestApplication.class, args);
    }

}
