CREATE TABLE album_permission (
    id INTEGER(99) primary key auto_increment,
    userId INTEGER(50) NOT NULL,
    albumId INTEGER(50) NOT NULL,
    allowRead BOOLEAN not null default false,
    allowWrite BOOLEAN not null default false
);
