package com.wolox.woloxtest.main.application.user;

import com.wolox.woloxtest.main.application.album.AlbumManager;
import com.wolox.woloxtest.main.application.album.adapter.Album;
import com.wolox.woloxtest.main.application.album.adapter.AlbumRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.util.List;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AlbumManagerTest {
    @Mock
    private AlbumRepository albumRepository;

    private AlbumManager albumManager;

    @BeforeEach
    private void setup() {
        albumManager = new AlbumManager(albumRepository);
    }

    @Test
    public void getAllUsers() {
        Album mockResult = Album.builder().title("Album tittle").build();
        when(albumRepository.getAllAlbums()).thenReturn(Flux.just(mockResult));
        Flux<Album> result = albumManager.getAllAlbums();

        StepVerifier.create(result).expectNext(mockResult).verifyComplete();
    }

    @Test
    public void getAlbumsByUser() {
        Long userId = 1L;
        List<Album> mockResult = List.of(
                Album.builder().userId(1L).title("Album tittle 1").build(),
                Album.builder().userId(2L).title("Album tittle 2").build()
        );
        when(albumRepository.getAllAlbums()).thenReturn(Flux.fromIterable(mockResult));
        Flux<Album> result = albumManager.getAlbumsByUser(userId);

        StepVerifier.create(result).expectNext(Album.builder().userId(1L).title("Album tittle 1").build()).verifyComplete();
    }

}