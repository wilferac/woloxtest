package com.wolox.woloxtest.main.application.user;

import com.wolox.woloxtest.main.application.album.AlbumManager;
import com.wolox.woloxtest.main.application.album.adapter.Album;
import com.wolox.woloxtest.main.application.photo.PhotoManager;
import com.wolox.woloxtest.main.application.photo.adapter.Photo;
import com.wolox.woloxtest.main.application.photo.adapter.PhotoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PhotoManagerTest {
    @Mock
    private PhotoRepository photoRepository;
    @Mock
    private AlbumManager albumManager;

    private PhotoManager photoManager;

    @BeforeEach
    private void setup() {
        photoManager = new PhotoManager(photoRepository, albumManager);
    }

    @Test
    public void getAllUsers() {
        Photo mockResult = Photo.builder().title("photo tittle").build();
        when(photoRepository.getAllPhotos()).thenReturn(Flux.just(mockResult));
        Flux<Photo> result = photoManager.getAllPhotos();

        StepVerifier.create(result).expectNext(mockResult).verifyComplete();
    }

    @Test
    public void getPhotosByUser() {
        Long userId = 1L;
        when(photoRepository.getAllPhotos()).thenReturn(getMockPhotos());
        when(albumManager.getAlbumsByUser(userId)).thenReturn(getMockAlbums());

        Flux<Photo> result = photoManager.getPhotosByUser(userId);

        List<Photo> expectedResult = List.of(
                Photo.builder().albumId(1L).title("photo 1").build(),
                Photo.builder().albumId(3L).title("photo 3").build(),
                Photo.builder().albumId(3L).title("photo 3.1").build()
        );

        StepVerifier.create(result.collectList()).expectNext(expectedResult).verifyComplete();
    }

    private Flux<Album> getMockAlbums() {
        return Flux.just(
                Album.builder().id(1L).build(),
                Album.builder().id(3L).build()
        );
    }

    private Flux<Photo> getMockPhotos() {
        return Flux.just(
                Photo.builder().albumId(1L).title("photo 1").build(),
                Photo.builder().albumId(2L).title("photo 2").build(),
                Photo.builder().albumId(3L).title("photo 3").build(),
                Photo.builder().albumId(3L).title("photo 3.1").build(),
                Photo.builder().albumId(4L).title("photo 4").build()
        );
    }


    public Flux<Photo> getPhotosByUser(Long userId) {
        Mono<Map<Long, Album>> userAlbums = albumManager.getAlbumsByUser(userId).collectMap(album -> album.getId());
        Mono<List<Photo>> allPhotos = photoRepository.getAllPhotos().collectList();

        return Mono.zip(userAlbums, allPhotos).map(t -> {
            return t.getT2().stream()
                    .filter(photo -> t.getT1().containsKey(photo.getAlbumId()))
                    .collect(Collectors.toList());
        }).flatMapIterable(e -> e);
    }


}