package com.wolox.woloxtest.main.application.album.adapter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AlbumAdapterTest {
    @Mock
    private WebClient webClient;
    @Mock
    private WebClient.RequestHeadersUriSpec requestHeadersUriSpec;
    @Mock
    private WebClient.RequestBodySpec requestBodySpec;
    @Mock
    private WebClient.ResponseSpec responseSpec;

    private AlbumAdapter albumAdapter;

    @BeforeEach
    private void setup() {
        albumAdapter = new AlbumAdapter(webClient);
    }

    @Test
    public void getAllAlbums() {
        Album mockResult = Album.builder().title("default tittle").build();
        when(webClient.get()).thenReturn(requestHeadersUriSpec);
        when(requestHeadersUriSpec.uri("https://jsonplaceholder.typicode.com/albums")).thenReturn(requestBodySpec);
        when(requestBodySpec.retrieve()).thenReturn(responseSpec);
        when(responseSpec.bodyToFlux(Album.class)).thenReturn(Flux.just(mockResult));

        Flux<Album> result = albumAdapter.getAllAlbums();

        StepVerifier.create(result).expectNext(mockResult).verifyComplete();
    }

}