package com.wolox.woloxtest.main.application.album.adapter;

import com.wolox.woloxtest.main.application.user.adapter.User;
import com.wolox.woloxtest.main.application.user.adapter.UserAdapter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserAdapterTest {
    @Mock
    private WebClient webClient;
    @Mock
    private WebClient.RequestHeadersUriSpec requestHeadersUriSpec;
    @Mock
    private WebClient.RequestBodySpec requestBodySpec;
    @Mock
    private WebClient.ResponseSpec responseSpec;

    private UserAdapter userAdapter;

    @BeforeEach
    private void setup() {
        userAdapter = new UserAdapter(webClient);
    }

    @Test
    public void getAllUsers() {
        User mockResult = User.builder().name("Monic").build();
        when(webClient.get()).thenReturn(requestHeadersUriSpec);
        when(requestHeadersUriSpec.uri("https://jsonplaceholder.typicode.com/users")).thenReturn(requestBodySpec);
        when(requestBodySpec.retrieve()).thenReturn(responseSpec);
        when(responseSpec.bodyToFlux(User.class)).thenReturn(Flux.just(mockResult));

        Flux<User> result = userAdapter.getAllUsers();

        StepVerifier.create(result).expectNext(mockResult).verifyComplete();
    }

}