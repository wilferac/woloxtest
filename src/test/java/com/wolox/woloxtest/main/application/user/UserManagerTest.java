package com.wolox.woloxtest.main.application.user;

import com.wolox.woloxtest.main.application.user.adapter.User;
import com.wolox.woloxtest.main.application.user.adapter.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserManagerTest {
    @Mock
    private UserRepository userRepository;

    private UserManager userManager;

    @BeforeEach
    private void setup() {
        userManager = new UserManager(userRepository);
    }

    @Test
    public void getAllUsers() {
        User mockResult = User.builder().name("Monic").build();
        when(userRepository.getAllUsers()).thenReturn(Flux.just(mockResult));
        Flux<User> result = userManager.getAllUsers();

        StepVerifier.create(result).expectNext(mockResult).verifyComplete();
    }

}