package com.wolox.woloxtest.main.application.album.adapter;

import com.wolox.woloxtest.main.application.photo.adapter.Photo;
import com.wolox.woloxtest.main.application.photo.adapter.PhotoAdapter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PhotoAdapterTest {
    @Mock
    private WebClient webClient;
    @Mock
    private WebClient.RequestHeadersUriSpec requestHeadersUriSpec;
    @Mock
    private WebClient.RequestBodySpec requestBodySpec;
    @Mock
    private WebClient.ResponseSpec responseSpec;

    private PhotoAdapter photoAdapter;

    @BeforeEach
    private void setup() {
        photoAdapter = new PhotoAdapter(webClient);
    }

    @Test
    public void getAllPhotos() {
        Photo mockResult = Photo.builder().title("photo tittle").build();
        when(webClient.get()).thenReturn(requestHeadersUriSpec);
        when(requestHeadersUriSpec.uri("https://jsonplaceholder.typicode.com/photos")).thenReturn(requestBodySpec);
        when(requestBodySpec.retrieve()).thenReturn(responseSpec);
        when(responseSpec.bodyToFlux(Photo.class)).thenReturn(Flux.just(mockResult));

        Flux<Photo> result = photoAdapter.getAllPhotos();

        StepVerifier.create(result).expectNext(mockResult).verifyComplete();
    }

}